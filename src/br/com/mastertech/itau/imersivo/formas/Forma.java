package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Forma {
	List<Double> lados = new ArrayList<>();
	
	public void calcularArea() {
	
		lados = ObterLados.obter();
		
		if(lados.size() == 0) {
			System.out.println("Forma inválida!");
			
		}else if(lados.size() == 1) {
			Circulo circulo = new Circulo();
			circulo.setLados(lados);
			circulo.calcular();
		
		}else if(lados.size() == 2) {
			Quadrado quadrado = new Quadrado();
			quadrado.setLados(lados);
			quadrado.calcular();
		
		}else if(lados.size() == 3) {
			
			Triangulo triangulo = new Triangulo();
			triangulo.setLados(lados);
			triangulo.calcular();
	
		}else {
			System.out.println("Ops! Eu não conheço essa forma geometrica ¯\\_(⊙_ʖ⊙)_/¯");
		}
	}

	public void setLados(List<Double> lados) {
		this.lados = lados;
		
	}
		
}
