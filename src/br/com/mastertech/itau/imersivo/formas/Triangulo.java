package br.com.mastertech.itau.imersivo.formas;

public class Triangulo extends Forma{

	public void calcular() {
		
		double ladoA = lados.get(0);
		double ladoB = lados.get(1);
		double ladoC = lados.get(2);
		
		System.out.println("Eu identifiquei um triangulo!");

		if((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA) {
			double s = (ladoA + ladoB + ladoC) / 2;       
			double area = Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
			System.out.println("A área do triangulo é " + (area));	
		}else {
			System.out.println("Mas o triangulo informado era inválido :/");
		}

	}
	
}
