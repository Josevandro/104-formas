package br.com.mastertech.itau.imersivo.formas;

public class Circulo extends Forma {
	
	public void calcular() {
		
		System.out.println("Eu identifiquei um circulo!");
		System.out.println("A área do circulo é " + (Math.PI * Math.pow(lados.get(0), 2)));
	}
	
}
