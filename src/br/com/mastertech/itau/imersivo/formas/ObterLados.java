package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ObterLados {

	public static List<Double> obter() {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Olá! bem vindo ao calculador de área 3 mil!");
		
		System.out.println("Basta informar a medida de cada lado que eu te digo a área :)");
		System.out.println("Vamos começar!");
		System.out.println("");
		System.out.println("Obs: digite -1 se quiser parar de cadastrar lados!");
		System.out.println("");
		
		List<Double> lados = new ArrayList<>();
		
		boolean deveAdicionarNovoLado = true;
		while(deveAdicionarNovoLado) {
			System.out.println("Informe o tamanho do lado " + (lados.size() + 1));
			
			double tamanhoLado = Double.parseDouble(scanner.nextLine());
			
			if(tamanhoLado <= 0) {
				deveAdicionarNovoLado = false;
			}else {
				lados.add(tamanhoLado);
			}
		}
		return lados;
		
	}	
}
