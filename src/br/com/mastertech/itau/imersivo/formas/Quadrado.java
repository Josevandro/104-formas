package br.com.mastertech.itau.imersivo.formas;

public class Quadrado extends Forma{
	
	public void calcular() {
		
		String tipo = "";
		
		if (lados.get(0) == lados.get(1)) {
			tipo = "Quadrado";
		} else {
			tipo = "Retangulo";
		}
		
		System.out.println("Eu identifiquei um " + tipo);
		System.out.println("A área do " + tipo + " é " + (lados.get(0) * lados.get(1)));

	}
	
}
